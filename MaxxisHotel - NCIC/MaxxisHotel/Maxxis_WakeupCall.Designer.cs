﻿namespace MaxxisHotel
{
    partial class Maxxis_WakeupCall
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnl_Title = new System.Windows.Forms.Panel();
            this.lbl_WakupCall = new System.Windows.Forms.Label();
            this.pnl_GuestInfo = new System.Windows.Forms.Panel();
            this.lbl_RoomNumber_Output = new System.Windows.Forms.Label();
            this.lbl_GuestName_Output = new System.Windows.Forms.Label();
            this.lbl_RoomNumber = new System.Windows.Forms.Label();
            this.lbl_GuestName = new System.Windows.Forms.Label();
            this.pcb_WakeupCall = new System.Windows.Forms.PictureBox();
            this.btn_AddWakeupCall = new System.Windows.Forms.Button();
            this.btn_GoBack = new System.Windows.Forms.Button();
            this.pnl_Title.SuspendLayout();
            this.pnl_GuestInfo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pcb_WakeupCall)).BeginInit();
            this.SuspendLayout();
            // 
            // pnl_Title
            // 
            this.pnl_Title.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(237)))), ((int)(((byte)(234)))));
            this.pnl_Title.Controls.Add(this.lbl_WakupCall);
            this.pnl_Title.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnl_Title.Location = new System.Drawing.Point(0, 0);
            this.pnl_Title.Name = "pnl_Title";
            this.pnl_Title.Size = new System.Drawing.Size(547, 30);
            this.pnl_Title.TabIndex = 1;
            // 
            // lbl_WakupCall
            // 
            this.lbl_WakupCall.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_WakupCall.ForeColor = System.Drawing.Color.Black;
            this.lbl_WakupCall.Image = global::MaxxisHotel.Properties.Resources.Wake_up_Call;
            this.lbl_WakupCall.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lbl_WakupCall.Location = new System.Drawing.Point(3, 0);
            this.lbl_WakupCall.Name = "lbl_WakupCall";
            this.lbl_WakupCall.Size = new System.Drawing.Size(124, 30);
            this.lbl_WakupCall.TabIndex = 0;
            this.lbl_WakupCall.Text = "    Wake-up Call";
            this.lbl_WakupCall.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pnl_GuestInfo
            // 
            this.pnl_GuestInfo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.pnl_GuestInfo.Controls.Add(this.lbl_RoomNumber_Output);
            this.pnl_GuestInfo.Controls.Add(this.lbl_GuestName_Output);
            this.pnl_GuestInfo.Controls.Add(this.lbl_RoomNumber);
            this.pnl_GuestInfo.Controls.Add(this.lbl_GuestName);
            this.pnl_GuestInfo.Controls.Add(this.pcb_WakeupCall);
            this.pnl_GuestInfo.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnl_GuestInfo.Location = new System.Drawing.Point(0, 30);
            this.pnl_GuestInfo.Name = "pnl_GuestInfo";
            this.pnl_GuestInfo.Size = new System.Drawing.Size(547, 90);
            this.pnl_GuestInfo.TabIndex = 2;
            // 
            // lbl_RoomNumber_Output
            // 
            this.lbl_RoomNumber_Output.AutoSize = true;
            this.lbl_RoomNumber_Output.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_RoomNumber_Output.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(126)))), ((int)(((byte)(57)))));
            this.lbl_RoomNumber_Output.Location = new System.Drawing.Point(132, 50);
            this.lbl_RoomNumber_Output.Name = "lbl_RoomNumber_Output";
            this.lbl_RoomNumber_Output.Size = new System.Drawing.Size(0, 16);
            this.lbl_RoomNumber_Output.TabIndex = 6;
            // 
            // lbl_GuestName_Output
            // 
            this.lbl_GuestName_Output.AutoSize = true;
            this.lbl_GuestName_Output.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_GuestName_Output.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(126)))), ((int)(((byte)(57)))));
            this.lbl_GuestName_Output.Location = new System.Drawing.Point(132, 21);
            this.lbl_GuestName_Output.Name = "lbl_GuestName_Output";
            this.lbl_GuestName_Output.Size = new System.Drawing.Size(0, 16);
            this.lbl_GuestName_Output.TabIndex = 5;
            // 
            // lbl_RoomNumber
            // 
            this.lbl_RoomNumber.AutoSize = true;
            this.lbl_RoomNumber.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_RoomNumber.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(126)))), ((int)(((byte)(57)))));
            this.lbl_RoomNumber.Location = new System.Drawing.Point(12, 50);
            this.lbl_RoomNumber.Name = "lbl_RoomNumber";
            this.lbl_RoomNumber.Size = new System.Drawing.Size(54, 16);
            this.lbl_RoomNumber.TabIndex = 4;
            this.lbl_RoomNumber.Text = "ROOM:";
            // 
            // lbl_GuestName
            // 
            this.lbl_GuestName.AutoSize = true;
            this.lbl_GuestName.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_GuestName.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(126)))), ((int)(((byte)(57)))));
            this.lbl_GuestName.Location = new System.Drawing.Point(12, 21);
            this.lbl_GuestName.Name = "lbl_GuestName";
            this.lbl_GuestName.Size = new System.Drawing.Size(100, 16);
            this.lbl_GuestName.TabIndex = 3;
            this.lbl_GuestName.Text = "GUEST NAME:";
            // 
            // pcb_WakeupCall
            // 
            this.pcb_WakeupCall.Image = global::MaxxisHotel.Properties.Resources.Wake_up_Call_Icon;
            this.pcb_WakeupCall.Location = new System.Drawing.Point(440, 0);
            this.pcb_WakeupCall.Name = "pcb_WakeupCall";
            this.pcb_WakeupCall.Size = new System.Drawing.Size(107, 90);
            this.pcb_WakeupCall.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pcb_WakeupCall.TabIndex = 0;
            this.pcb_WakeupCall.TabStop = false;
            // 
            // btn_AddWakeupCall
            // 
            this.btn_AddWakeupCall.BackColor = System.Drawing.Color.DarkGreen;
            this.btn_AddWakeupCall.FlatAppearance.BorderSize = 0;
            this.btn_AddWakeupCall.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_AddWakeupCall.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_AddWakeupCall.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(237)))), ((int)(((byte)(234)))));
            this.btn_AddWakeupCall.Location = new System.Drawing.Point(135, 206);
            this.btn_AddWakeupCall.Name = "btn_AddWakeupCall";
            this.btn_AddWakeupCall.Size = new System.Drawing.Size(258, 32);
            this.btn_AddWakeupCall.TabIndex = 3;
            this.btn_AddWakeupCall.Text = "ADD WAKE-UP CALL";
            this.btn_AddWakeupCall.UseVisualStyleBackColor = false;
            // 
            // btn_GoBack
            // 
            this.btn_GoBack.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_GoBack.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btn_GoBack.FlatAppearance.BorderSize = 0;
            this.btn_GoBack.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_GoBack.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_GoBack.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_GoBack.ForeColor = System.Drawing.Color.White;
            this.btn_GoBack.Image = global::MaxxisHotel.Properties.Resources.Go_Back;
            this.btn_GoBack.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_GoBack.Location = new System.Drawing.Point(211, 327);
            this.btn_GoBack.Margin = new System.Windows.Forms.Padding(3, 3, 3, 0);
            this.btn_GoBack.Name = "btn_GoBack";
            this.btn_GoBack.Size = new System.Drawing.Size(109, 25);
            this.btn_GoBack.TabIndex = 4;
            this.btn_GoBack.Text = "GO BACK";
            this.btn_GoBack.UseVisualStyleBackColor = false;
            this.btn_GoBack.Click += new System.EventHandler(this.btn_GoBack_Click);
            // 
            // Maxxis_WakeupCall
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(237)))), ((int)(((byte)(234)))));
            this.ClientSize = new System.Drawing.Size(547, 361);
            this.Controls.Add(this.btn_GoBack);
            this.Controls.Add(this.btn_AddWakeupCall);
            this.Controls.Add(this.pnl_GuestInfo);
            this.Controls.Add(this.pnl_Title);
            this.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Maxxis_WakeupCall";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Wake-up Call";
            this.Load += new System.EventHandler(this.Maxxis_WakeupCall_Load);
            this.pnl_Title.ResumeLayout(false);
            this.pnl_GuestInfo.ResumeLayout(false);
            this.pnl_GuestInfo.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pcb_WakeupCall)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lbl_WakupCall;
        private System.Windows.Forms.Panel pnl_Title;
        private System.Windows.Forms.Panel pnl_GuestInfo;
        private System.Windows.Forms.Label lbl_RoomNumber_Output;
        private System.Windows.Forms.Label lbl_GuestName_Output;
        private System.Windows.Forms.Label lbl_RoomNumber;
        private System.Windows.Forms.Label lbl_GuestName;
        private System.Windows.Forms.PictureBox pcb_WakeupCall;
        private System.Windows.Forms.Button btn_AddWakeupCall;
        private System.Windows.Forms.Button btn_GoBack;
    }
}