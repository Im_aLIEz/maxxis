﻿namespace MaxxisHotel
{
    partial class Maxxis_RoomInfo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbl_MaxxHotel = new System.Windows.Forms.Label();
            this.lbl_Location = new System.Windows.Forms.Label();
            this.pnl_title = new System.Windows.Forms.Panel();
            this.lbl_Address = new System.Windows.Forms.Label();
            this.pnl_RoomDetails = new System.Windows.Forms.Panel();
            this.vscr_RoomDetails = new System.Windows.Forms.VScrollBar();
            this.pnl_RoomInformation = new System.Windows.Forms.Panel();
            this.btn_GoBack = new System.Windows.Forms.Button();
            this.lbl_RoomDetails = new System.Windows.Forms.Label();
            this.pcb_Logo = new System.Windows.Forms.PictureBox();
            this.pnl_title.SuspendLayout();
            this.pnl_RoomDetails.SuspendLayout();
            this.pnl_RoomInformation.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pcb_Logo)).BeginInit();
            this.SuspendLayout();
            // 
            // lbl_MaxxHotel
            // 
            this.lbl_MaxxHotel.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ Extended", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_MaxxHotel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(126)))), ((int)(((byte)(57)))));
            this.lbl_MaxxHotel.Location = new System.Drawing.Point(271, 15);
            this.lbl_MaxxHotel.Name = "lbl_MaxxHotel";
            this.lbl_MaxxHotel.Size = new System.Drawing.Size(388, 33);
            this.lbl_MaxxHotel.TabIndex = 0;
            this.lbl_MaxxHotel.Text = "MAXX HOTEL MAKATI";
            // 
            // lbl_Location
            // 
            this.lbl_Location.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 9.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_Location.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(237)))), ((int)(((byte)(234)))));
            this.lbl_Location.Location = new System.Drawing.Point(274, 48);
            this.lbl_Location.Name = "lbl_Location";
            this.lbl_Location.Size = new System.Drawing.Size(374, 20);
            this.lbl_Location.TabIndex = 2;
            this.lbl_Location.Text = "Makati Avenue corner Singian Street,";
            // 
            // pnl_title
            // 
            this.pnl_title.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.pnl_title.Controls.Add(this.lbl_Address);
            this.pnl_title.Controls.Add(this.pcb_Logo);
            this.pnl_title.Controls.Add(this.lbl_Location);
            this.pnl_title.Controls.Add(this.lbl_MaxxHotel);
            this.pnl_title.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnl_title.Location = new System.Drawing.Point(0, 0);
            this.pnl_title.Name = "pnl_title";
            this.pnl_title.Size = new System.Drawing.Size(1200, 100);
            this.pnl_title.TabIndex = 3;
            // 
            // lbl_Address
            // 
            this.lbl_Address.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 9.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_Address.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(237)))), ((int)(((byte)(234)))));
            this.lbl_Address.Location = new System.Drawing.Point(274, 68);
            this.lbl_Address.Name = "lbl_Address";
            this.lbl_Address.Size = new System.Drawing.Size(374, 20);
            this.lbl_Address.TabIndex = 3;
            this.lbl_Address.Text = "Makati City, 1210 PHILIPPINES";
            // 
            // pnl_RoomDetails
            // 
            this.pnl_RoomDetails.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(172)))), ((int)(((byte)(172)))), ((int)(((byte)(172)))));
            this.pnl_RoomDetails.Controls.Add(this.vscr_RoomDetails);
            this.pnl_RoomDetails.Controls.Add(this.pnl_RoomInformation);
            this.pnl_RoomDetails.Location = new System.Drawing.Point(105, 117);
            this.pnl_RoomDetails.Name = "pnl_RoomDetails";
            this.pnl_RoomDetails.Size = new System.Drawing.Size(1005, 544);
            this.pnl_RoomDetails.TabIndex = 4;
            // 
            // vscr_RoomDetails
            // 
            this.vscr_RoomDetails.Dock = System.Windows.Forms.DockStyle.Right;
            this.vscr_RoomDetails.Location = new System.Drawing.Point(988, 33);
            this.vscr_RoomDetails.Name = "vscr_RoomDetails";
            this.vscr_RoomDetails.Size = new System.Drawing.Size(17, 511);
            this.vscr_RoomDetails.TabIndex = 1;
            // 
            // pnl_RoomInformation
            // 
            this.pnl_RoomInformation.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.pnl_RoomInformation.Controls.Add(this.lbl_RoomDetails);
            this.pnl_RoomInformation.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnl_RoomInformation.Location = new System.Drawing.Point(0, 0);
            this.pnl_RoomInformation.Name = "pnl_RoomInformation";
            this.pnl_RoomInformation.Size = new System.Drawing.Size(1005, 33);
            this.pnl_RoomInformation.TabIndex = 0;
            // 
            // btn_GoBack
            // 
            this.btn_GoBack.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_GoBack.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btn_GoBack.FlatAppearance.BorderSize = 0;
            this.btn_GoBack.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_GoBack.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_GoBack.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_GoBack.ForeColor = System.Drawing.Color.White;
            this.btn_GoBack.Image = global::MaxxisHotel.Properties.Resources.Go_Back;
            this.btn_GoBack.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_GoBack.Location = new System.Drawing.Point(590, 676);
            this.btn_GoBack.Margin = new System.Windows.Forms.Padding(3, 3, 3, 0);
            this.btn_GoBack.Name = "btn_GoBack";
            this.btn_GoBack.Size = new System.Drawing.Size(109, 25);
            this.btn_GoBack.TabIndex = 3;
            this.btn_GoBack.Text = "GO BACK";
            this.btn_GoBack.UseVisualStyleBackColor = false;
            this.btn_GoBack.Click += new System.EventHandler(this.btn_GoBack_Click);
            // 
            // lbl_RoomDetails
            // 
            this.lbl_RoomDetails.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 9.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_RoomDetails.ForeColor = System.Drawing.Color.White;
            this.lbl_RoomDetails.Image = global::MaxxisHotel.Properties.Resources.Room_Information;
            this.lbl_RoomDetails.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lbl_RoomDetails.Location = new System.Drawing.Point(3, 0);
            this.lbl_RoomDetails.Name = "lbl_RoomDetails";
            this.lbl_RoomDetails.Size = new System.Drawing.Size(157, 33);
            this.lbl_RoomDetails.TabIndex = 3;
            this.lbl_RoomDetails.Text = "ROOM DETAILS";
            this.lbl_RoomDetails.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pcb_Logo
            // 
            this.pcb_Logo.Location = new System.Drawing.Point(148, 3);
            this.pcb_Logo.Name = "pcb_Logo";
            this.pcb_Logo.Size = new System.Drawing.Size(117, 94);
            this.pcb_Logo.TabIndex = 1;
            this.pcb_Logo.TabStop = false;
            // 
            // Maxxis_RoomInfo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(237)))), ((int)(((byte)(234)))));
            this.ClientSize = new System.Drawing.Size(1200, 710);
            this.Controls.Add(this.btn_GoBack);
            this.Controls.Add(this.pnl_RoomDetails);
            this.Controls.Add(this.pnl_title);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Maxxis_RoomInfo";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Maxxis_RoomInfo";
            this.pnl_title.ResumeLayout(false);
            this.pnl_RoomDetails.ResumeLayout(false);
            this.pnl_RoomInformation.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pcb_Logo)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lbl_MaxxHotel;
        private System.Windows.Forms.PictureBox pcb_Logo;
        private System.Windows.Forms.Label lbl_Location;
        private System.Windows.Forms.Panel pnl_title;
        private System.Windows.Forms.Button btn_GoBack;
        private System.Windows.Forms.Panel pnl_RoomDetails;
        private System.Windows.Forms.Label lbl_Address;
        private System.Windows.Forms.VScrollBar vscr_RoomDetails;
        private System.Windows.Forms.Panel pnl_RoomInformation;
        private System.Windows.Forms.Label lbl_RoomDetails;
    }
}