﻿namespace MaxxisHotel
{
    partial class Maxxis_GuestRequest
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            this.pnl_GuestRequest = new System.Windows.Forms.Panel();
            this.lbl_GuestRequest = new System.Windows.Forms.Label();
            this.pcb_GuestRequestIcon = new System.Windows.Forms.PictureBox();
            this.pnl_Room_FolioNumber = new System.Windows.Forms.Panel();
            this.pnl_dataGrid = new System.Windows.Forms.Panel();
            this.btn_AddNew = new System.Windows.Forms.Button();
            this.btn_Back = new System.Windows.Forms.Button();
            this.dgv_GuestRequest = new System.Windows.Forms.DataGridView();
            this.colDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colItem = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colQuantity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colUOM = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colRemarks = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colStatus = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lbl_RoomNumber = new System.Windows.Forms.Label();
            this.lbl_FolioNumber = new System.Windows.Forms.Label();
            this.txt_RoomNumber = new System.Windows.Forms.TextBox();
            this.txt_FolioNumber = new System.Windows.Forms.TextBox();
            this.pnl_GuestRequest.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pcb_GuestRequestIcon)).BeginInit();
            this.pnl_Room_FolioNumber.SuspendLayout();
            this.pnl_dataGrid.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_GuestRequest)).BeginInit();
            this.SuspendLayout();
            // 
            // pnl_GuestRequest
            // 
            this.pnl_GuestRequest.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.pnl_GuestRequest.Controls.Add(this.btn_Back);
            this.pnl_GuestRequest.Controls.Add(this.btn_AddNew);
            this.pnl_GuestRequest.Controls.Add(this.lbl_GuestRequest);
            this.pnl_GuestRequest.Controls.Add(this.pcb_GuestRequestIcon);
            this.pnl_GuestRequest.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnl_GuestRequest.Location = new System.Drawing.Point(0, 0);
            this.pnl_GuestRequest.Name = "pnl_GuestRequest";
            this.pnl_GuestRequest.Size = new System.Drawing.Size(800, 58);
            this.pnl_GuestRequest.TabIndex = 0;
            // 
            // lbl_GuestRequest
            // 
            this.lbl_GuestRequest.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.lbl_GuestRequest.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_GuestRequest.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(126)))), ((int)(((byte)(57)))));
            this.lbl_GuestRequest.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lbl_GuestRequest.Location = new System.Drawing.Point(58, 5);
            this.lbl_GuestRequest.Name = "lbl_GuestRequest";
            this.lbl_GuestRequest.Size = new System.Drawing.Size(163, 48);
            this.lbl_GuestRequest.TabIndex = 1;
            this.lbl_GuestRequest.Text = "Guest Request";
            this.lbl_GuestRequest.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // pcb_GuestRequestIcon
            // 
            this.pcb_GuestRequestIcon.Dock = System.Windows.Forms.DockStyle.Left;
            this.pcb_GuestRequestIcon.Image = global::MaxxisHotel.Properties.Resources.Guest_Request_2;
            this.pcb_GuestRequestIcon.Location = new System.Drawing.Point(0, 0);
            this.pcb_GuestRequestIcon.Name = "pcb_GuestRequestIcon";
            this.pcb_GuestRequestIcon.Size = new System.Drawing.Size(63, 58);
            this.pcb_GuestRequestIcon.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pcb_GuestRequestIcon.TabIndex = 2;
            this.pcb_GuestRequestIcon.TabStop = false;
            // 
            // pnl_Room_FolioNumber
            // 
            this.pnl_Room_FolioNumber.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(237)))), ((int)(((byte)(234)))));
            this.pnl_Room_FolioNumber.Controls.Add(this.txt_FolioNumber);
            this.pnl_Room_FolioNumber.Controls.Add(this.txt_RoomNumber);
            this.pnl_Room_FolioNumber.Controls.Add(this.lbl_FolioNumber);
            this.pnl_Room_FolioNumber.Controls.Add(this.lbl_RoomNumber);
            this.pnl_Room_FolioNumber.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnl_Room_FolioNumber.Location = new System.Drawing.Point(0, 540);
            this.pnl_Room_FolioNumber.Name = "pnl_Room_FolioNumber";
            this.pnl_Room_FolioNumber.Size = new System.Drawing.Size(800, 94);
            this.pnl_Room_FolioNumber.TabIndex = 2;
            // 
            // pnl_dataGrid
            // 
            this.pnl_dataGrid.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(237)))), ((int)(((byte)(234)))));
            this.pnl_dataGrid.Controls.Add(this.dgv_GuestRequest);
            this.pnl_dataGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnl_dataGrid.Location = new System.Drawing.Point(0, 58);
            this.pnl_dataGrid.Name = "pnl_dataGrid";
            this.pnl_dataGrid.Size = new System.Drawing.Size(800, 482);
            this.pnl_dataGrid.TabIndex = 3;
            // 
            // btn_AddNew
            // 
            this.btn_AddNew.BackColor = System.Drawing.Color.DarkGreen;
            this.btn_AddNew.FlatAppearance.BorderSize = 0;
            this.btn_AddNew.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.btn_AddNew.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(172)))), ((int)(((byte)(172)))), ((int)(((byte)(172)))));
            this.btn_AddNew.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_AddNew.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_AddNew.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(237)))), ((int)(((byte)(234)))));
            this.btn_AddNew.Location = new System.Drawing.Point(561, 13);
            this.btn_AddNew.Name = "btn_AddNew";
            this.btn_AddNew.Size = new System.Drawing.Size(117, 36);
            this.btn_AddNew.TabIndex = 3;
            this.btn_AddNew.Text = "ADD NEW";
            this.btn_AddNew.UseVisualStyleBackColor = false;
            this.btn_AddNew.Click += new System.EventHandler(this.btn_AddNew_Click);
            // 
            // btn_Back
            // 
            this.btn_Back.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.btn_Back.FlatAppearance.BorderSize = 0;
            this.btn_Back.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.btn_Back.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            this.btn_Back.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_Back.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Back.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(237)))), ((int)(((byte)(234)))));
            this.btn_Back.Location = new System.Drawing.Point(684, 13);
            this.btn_Back.Name = "btn_Back";
            this.btn_Back.Size = new System.Drawing.Size(104, 36);
            this.btn_Back.TabIndex = 4;
            this.btn_Back.Text = "BACK";
            this.btn_Back.UseVisualStyleBackColor = false;
            this.btn_Back.Click += new System.EventHandler(this.btn_Back_Click);
            // 
            // dgv_GuestRequest
            // 
            this.dgv_GuestRequest.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.Padding = new System.Windows.Forms.Padding(0, 7, 0, 7);
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_GuestRequest.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgv_GuestRequest.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_GuestRequest.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colDate,
            this.colItem,
            this.colQuantity,
            this.colUOM,
            this.colRemarks,
            this.colStatus});
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgv_GuestRequest.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgv_GuestRequest.EnableHeadersVisualStyles = false;
            this.dgv_GuestRequest.Location = new System.Drawing.Point(12, 18);
            this.dgv_GuestRequest.Name = "dgv_GuestRequest";
            this.dgv_GuestRequest.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(51)))));
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.Info;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_GuestRequest.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dgv_GuestRequest.RowHeadersVisible = false;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            this.dgv_GuestRequest.RowsDefaultCellStyle = dataGridViewCellStyle4;
            this.dgv_GuestRequest.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dgv_GuestRequest.Size = new System.Drawing.Size(776, 446);
            this.dgv_GuestRequest.TabIndex = 0;
            // 
            // colDate
            // 
            this.colDate.HeaderText = "Date";
            this.colDate.Name = "colDate";
            this.colDate.ReadOnly = true;
            // 
            // colItem
            // 
            this.colItem.HeaderText = "Item";
            this.colItem.Name = "colItem";
            this.colItem.ReadOnly = true;
            this.colItem.Width = 250;
            // 
            // colQuantity
            // 
            this.colQuantity.HeaderText = "Qty.";
            this.colQuantity.Name = "colQuantity";
            this.colQuantity.ReadOnly = true;
            this.colQuantity.Width = 50;
            // 
            // colUOM
            // 
            this.colUOM.HeaderText = "UOM";
            this.colUOM.Name = "colUOM";
            this.colUOM.ReadOnly = true;
            this.colUOM.Width = 80;
            // 
            // colRemarks
            // 
            this.colRemarks.HeaderText = "Remarks";
            this.colRemarks.Name = "colRemarks";
            this.colRemarks.ReadOnly = true;
            this.colRemarks.Width = 190;
            // 
            // colStatus
            // 
            this.colStatus.HeaderText = "Status";
            this.colStatus.Name = "colStatus";
            this.colStatus.ReadOnly = true;
            this.colStatus.Width = 105;
            // 
            // lbl_RoomNumber
            // 
            this.lbl_RoomNumber.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_RoomNumber.ForeColor = System.Drawing.Color.Black;
            this.lbl_RoomNumber.Location = new System.Drawing.Point(12, 14);
            this.lbl_RoomNumber.Name = "lbl_RoomNumber";
            this.lbl_RoomNumber.Size = new System.Drawing.Size(100, 23);
            this.lbl_RoomNumber.TabIndex = 0;
            this.lbl_RoomNumber.Text = "@ Room";
            // 
            // lbl_FolioNumber
            // 
            this.lbl_FolioNumber.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_FolioNumber.ForeColor = System.Drawing.Color.Black;
            this.lbl_FolioNumber.Location = new System.Drawing.Point(12, 51);
            this.lbl_FolioNumber.Name = "lbl_FolioNumber";
            this.lbl_FolioNumber.Size = new System.Drawing.Size(106, 23);
            this.lbl_FolioNumber.TabIndex = 1;
            this.lbl_FolioNumber.Text = "@ Folio No.";
            // 
            // txt_RoomNumber
            // 
            this.txt_RoomNumber.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_RoomNumber.Location = new System.Drawing.Point(154, 16);
            this.txt_RoomNumber.Name = "txt_RoomNumber";
            this.txt_RoomNumber.Size = new System.Drawing.Size(157, 23);
            this.txt_RoomNumber.TabIndex = 2;
            this.txt_RoomNumber.Text = "0";
            this.txt_RoomNumber.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txt_FolioNumber
            // 
            this.txt_FolioNumber.Font = new System.Drawing.Font("Akzidenz-Grotesk BQ", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_FolioNumber.Location = new System.Drawing.Point(154, 53);
            this.txt_FolioNumber.Name = "txt_FolioNumber";
            this.txt_FolioNumber.Size = new System.Drawing.Size(157, 23);
            this.txt_FolioNumber.TabIndex = 3;
            this.txt_FolioNumber.Text = "0";
            this.txt_FolioNumber.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // Maxxis_GuestRequest
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 634);
            this.Controls.Add(this.pnl_dataGrid);
            this.Controls.Add(this.pnl_Room_FolioNumber);
            this.Controls.Add(this.pnl_GuestRequest);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Maxxis_GuestRequest";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Maxxis_GuestRequest";
            this.pnl_GuestRequest.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pcb_GuestRequestIcon)).EndInit();
            this.pnl_Room_FolioNumber.ResumeLayout(false);
            this.pnl_Room_FolioNumber.PerformLayout();
            this.pnl_dataGrid.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_GuestRequest)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnl_GuestRequest;
        private System.Windows.Forms.Label lbl_GuestRequest;
        private System.Windows.Forms.PictureBox pcb_GuestRequestIcon;
        private System.Windows.Forms.Panel pnl_Room_FolioNumber;
        private System.Windows.Forms.Panel pnl_dataGrid;
        private System.Windows.Forms.Button btn_Back;
        private System.Windows.Forms.Button btn_AddNew;
        private System.Windows.Forms.Label lbl_FolioNumber;
        private System.Windows.Forms.Label lbl_RoomNumber;
        private System.Windows.Forms.DataGridView dgv_GuestRequest;
        private System.Windows.Forms.DataGridViewTextBoxColumn colDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn colItem;
        private System.Windows.Forms.DataGridViewTextBoxColumn colQuantity;
        private System.Windows.Forms.DataGridViewTextBoxColumn colUOM;
        private System.Windows.Forms.DataGridViewTextBoxColumn colRemarks;
        private System.Windows.Forms.DataGridViewTextBoxColumn colStatus;
        private System.Windows.Forms.TextBox txt_FolioNumber;
        private System.Windows.Forms.TextBox txt_RoomNumber;
    }
}