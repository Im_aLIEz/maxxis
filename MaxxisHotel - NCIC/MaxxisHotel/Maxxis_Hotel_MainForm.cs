﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MaxxisHotel
{
    public partial class Maxxis_Hotel_MainForm : Form
    {
        public Maxxis_Hotel_MainForm()
        {
            InitializeComponent();
            //Default Position of the Left Yellow Selector
            pnl_FloorMenu_Selector.Height = btn_All_Floors.Height;
            pnl_FloorMenu_Selector.Top = btn_All_Floors.Top;

            //Default Position of the Right LightBlue Selector
            pnl_MenuSelector.Height = btn_RoomInformation.Height;
            pnl_MenuSelector.Top = btn_RoomInformation.Top;
        }

        private void lbl_FloorMenu_Click(object sender, EventArgs e)
        {

        }

        private void Maxxis_Hotel_MainForm_Load(object sender, EventArgs e)
        {

        }

        private void lbl_OpenMenu_Click(object sender, EventArgs e)
        {

        }

        private void btn_All_Floors_Click(object sender, EventArgs e)
        {
            //Changes The Position of the left selector
            pnl_FloorMenu_Selector.Height = btn_All_Floors.Height;
            pnl_FloorMenu_Selector.Top = btn_All_Floors.Top;

            //this will make the floors selected visible
            pnl_3rdfloor.Visible = true;
            pnl_2ndfloor.Visible = true;
            pnl_5thfloor.Visible = true;
            pnl_6thfloor.Visible = true;
            pnl_7thfloor.Visible = true;
            pnl_8thfloor.Visible = true;
            pnl_9thfloor.Visible = true;
            pnl_10thfloor.Visible = true;
        }

        private void btn_10th_Floor_Click(object sender, EventArgs e)
        {
            //Changes The Position of the left selector
            pnl_FloorMenu_Selector.Height = btn_10th_Floor.Height;
            pnl_FloorMenu_Selector.Top = btn_10th_Floor.Top;

            //this will make the floors selected visible
            pnl_10thfloor.Visible = true;

            pnl_2ndfloor.Visible = false;
            pnl_5thfloor.Visible = false;
            pnl_6thfloor.Visible = false;
            pnl_7thfloor.Visible = false;
            pnl_8thfloor.Visible = false;
            pnl_9thfloor.Visible = false;
            pnl_3rdfloor.Visible = false;
        }

        private void btn_9th_Floor_Click(object sender, EventArgs e)
        {
            //Changes The Position of the left selector
            pnl_FloorMenu_Selector.Height = btn_9th_Floor.Height;
            pnl_FloorMenu_Selector.Top = btn_9th_Floor.Top;

            //this will make the floors selected visible
            pnl_9thfloor.Visible = true;

            pnl_2ndfloor.Visible = false;
            pnl_5thfloor.Visible = false;
            pnl_6thfloor.Visible = false;
            pnl_7thfloor.Visible = false;
            pnl_8thfloor.Visible = false;
            pnl_3rdfloor.Visible = false;
            pnl_10thfloor.Visible = false;
        }

        private void btn_8th_Floor_Click(object sender, EventArgs e)
        {
            //Changes The Position of the left selector
            pnl_FloorMenu_Selector.Height = btn_8th_Floor.Height;
            pnl_FloorMenu_Selector.Top = btn_8th_Floor.Top;

            //this will make the floors selected visible
            pnl_8thfloor.Visible = true;

            pnl_2ndfloor.Visible = false;
            pnl_5thfloor.Visible = false;
            pnl_6thfloor.Visible = false;
            pnl_7thfloor.Visible = false;
            pnl_3rdfloor.Visible = false;
            pnl_9thfloor.Visible = false;
            pnl_10thfloor.Visible = false;
        }

        private void btn_7th_Floor_Click(object sender, EventArgs e)
        {
            //Changes The Position of the left selector
            pnl_FloorMenu_Selector.Height = btn_7th_Floor.Height;
            pnl_FloorMenu_Selector.Top = btn_7th_Floor.Top;

            //this will make the floors selected visible
            pnl_7thfloor.Visible = true;

            pnl_2ndfloor.Visible = false;
            pnl_5thfloor.Visible = false;
            pnl_6thfloor.Visible = false;
            pnl_3rdfloor.Visible = false;
            pnl_8thfloor.Visible = false;
            pnl_9thfloor.Visible = false;
            pnl_10thfloor.Visible = false;
        }

        private void btn_6th_Floor_Click(object sender, EventArgs e)
        {
            //Changes The Position of the left selector
            pnl_FloorMenu_Selector.Height = btn_6th_Floor.Height;
            pnl_FloorMenu_Selector.Top = btn_6th_Floor.Top;

            //this will make the floors selected visible
            pnl_6thfloor.Visible = true;

            pnl_2ndfloor.Visible = false;
            pnl_5thfloor.Visible = false;
            pnl_3rdfloor.Visible = false;
            pnl_7thfloor.Visible = false;
            pnl_8thfloor.Visible = false;
            pnl_9thfloor.Visible = false;
            pnl_10thfloor.Visible = false;
        }

        private void btn_5th_Floor_Click(object sender, EventArgs e)
        {
            //Changes The Position of the left selector
            pnl_FloorMenu_Selector.Height = btn_5th_Floor.Height;
            pnl_FloorMenu_Selector.Top = btn_5th_Floor.Top;

            //this will make the floors selected visible
            pnl_5thfloor.Visible = true;

            pnl_2ndfloor.Visible = false;
            pnl_3rdfloor.Visible = false;
            pnl_6thfloor.Visible = false;
            pnl_7thfloor.Visible = false;
            pnl_8thfloor.Visible = false;
            pnl_9thfloor.Visible = false;
            pnl_10thfloor.Visible = false;
        }

        private void btn_3rd_Floor_Click(object sender, EventArgs e)
        {
            //Changes The Position of the left selector
            pnl_FloorMenu_Selector.Height = btn_3rd_Floor.Height;
            pnl_FloorMenu_Selector.Top = btn_3rd_Floor.Top;

            //this will make the floors selected visible
            pnl_3rdfloor.Visible = true;

            pnl_2ndfloor.Visible = false;
            pnl_5thfloor.Visible = false;
            pnl_6thfloor.Visible = false;
            pnl_7thfloor.Visible = false;
            pnl_8thfloor.Visible = false;
            pnl_9thfloor.Visible = false;
            pnl_10thfloor.Visible = false;
        }

        private void btn_2nd_Floor_Click(object sender, EventArgs e)
        {
            //Changes The Position of the left selector
            pnl_FloorMenu_Selector.Height = btn_2nd_Floor.Height;
            pnl_FloorMenu_Selector.Top = btn_2nd_Floor.Top;

            //this will make the floors selected visible
            pnl_2ndfloor.Visible = true;
            pnl_3rdfloor.Visible = false;
            pnl_5thfloor.Visible = false;
            pnl_6thfloor.Visible = false;
            pnl_7thfloor.Visible = false;
            pnl_8thfloor.Visible = false;
            pnl_9thfloor.Visible = false;
            pnl_10thfloor.Visible = false;
        }

        private void btn_RoomInformation_Click(object sender, EventArgs e)
        {
            //Changes The Position of the Right selector
            pnl_MenuSelector.Height = btn_RoomInformation.Height;
            pnl_MenuSelector.Top = btn_RoomInformation.Top;

            //Shows The room Information
            Maxxis_RoomInfo MRI = new Maxxis_RoomInfo();
            this.Hide();
            MRI.ShowDialog();
            this.Show();
        }

        private void btn_GuestFolio_Click(object sender, EventArgs e)
        {
            //Changes The Position of the Right selector
            pnl_MenuSelector.Height = btn_GuestFolio.Height;
            pnl_MenuSelector.Top = btn_GuestFolio.Top;
        }

        private void btn_GuestMessage_Click(object sender, EventArgs e)
        {
            //Changes The Position of the Right selector
            pnl_MenuSelector.Height = btn_GuestMessage.Height;
            pnl_MenuSelector.Top = btn_GuestMessage.Top;
        }

        private void btn_WakeupCall_Click(object sender, EventArgs e)
        {
            //Changes The Position of the Right selector
            pnl_MenuSelector.Height = btn_WakeupCall.Height;
            pnl_MenuSelector.Top = btn_WakeupCall.Top;

            //Shows the Wake-up Call form
            Maxxis_WakeupCall MWC = new Maxxis_WakeupCall();
            this.Hide();
            MWC.ShowDialog();
            this.Show();
        }

        private void btn_GuestRequest_Click(object sender, EventArgs e)
        {
            //Changes The Position of the Right selector
            pnl_MenuSelector.Height = btn_GuestRequest.Height;
            pnl_MenuSelector.Top = btn_GuestRequest.Top;

            //Shows the Guest Request Form
            Maxxis_GuestRequest MGR = new Maxxis_GuestRequest();
            this.Hide();
            MGR.ShowDialog();
            this.Show();
        }

        private void btn_Loss_Breakage_Fines_Click(object sender, EventArgs e)
        {
            //Changes The Position of the Right selector
            pnl_MenuSelector.Height = btn_Loss_Breakage_Fines.Height;
            pnl_MenuSelector.Top = btn_Loss_Breakage_Fines.Top;
        }

        private void btn_TransferRoom_Click(object sender, EventArgs e)
        {
            //Changes The Position of the Right selector
            pnl_MenuSelector.Height = btn_TransferRoom.Height;
            pnl_MenuSelector.Top = btn_TransferRoom.Top;
        }

        private void btn_CheckOut_Click(object sender, EventArgs e)
        {
            //Changes The Position of the Right selector
            pnl_MenuSelector.Height = btn_CheckOut.Height;
            pnl_MenuSelector.Top = btn_CheckOut.Top;
        }

        private void btn_NewReservation_Click(object sender, EventArgs e)
        {
            //Changes The Position of the Right selector
            pnl_MenuSelector.Height = btn_NewReservation.Height;
            pnl_MenuSelector.Top = btn_NewReservation.Top;
        }

        private void btn_New_Walkin_Guest_Click(object sender, EventArgs e)
        {
            //Changes The Position of the Right selector
            pnl_MenuSelector.Height = btn_New_Walkin_Guest.Height;
            pnl_MenuSelector.Top = btn_New_Walkin_Guest.Top;
        }

        private void btn_GuestArrivalList_Click(object sender, EventArgs e)
        {
            //Changes The Position of the Right selector
            pnl_MenuSelector.Height = btn_GuestArrivalList.Height;
            pnl_MenuSelector.Top = btn_GuestArrivalList.Top;

            //Shows the Guest Arrival List form
            Maxxis_GuestArrivalList MGAL = new Maxxis_GuestArrivalList();
            this.Hide();
            MGAL.ShowDialog();
            this.Show();

        }

        private void btn_Reports_Click(object sender, EventArgs e)
        {
            //Changes The Position of the Right selector
            pnl_MenuSelector.Height = btn_Reports.Height;
            pnl_MenuSelector.Top = btn_Reports.Top;
        }

        private void btn_FrontDesk_Click(object sender, EventArgs e)
        {
            //Changes The Position of the Right selector
            pnl_MenuSelector.Height = btn_FrontDesk.Height;
            pnl_MenuSelector.Top = btn_FrontDesk.Top;
        }

        private void btn_SignOut_Click(object sender, EventArgs e)
        {
            //Changes The Position of the Right selector
            pnl_MenuSelector.Height = btn_SignOut.Height;
            pnl_MenuSelector.Top = btn_SignOut.Top;

            //sign-out
            //Maxxis_Hotel_LoginForm newform = new Maxxis_Hotel_LoginForm();
            //newform.Show();
            this.Close();
        }
    }
}
