﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MaxxisHotel
{
    public partial class Maxxis_RoomInfo : Form
    {
        public Maxxis_RoomInfo()
        {
            InitializeComponent();
        }

        private void btn_GoBack_Click(object sender, EventArgs e)
        {
            //Go Back to the main form
            this.Close();
        }
    }
}
