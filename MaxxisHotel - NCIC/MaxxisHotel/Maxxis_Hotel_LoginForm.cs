﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MaxxisHotel
{
    public partial class Maxxis_Hotel_LoginForm : Form
    {
        public Maxxis_Hotel_LoginForm()
        {
            InitializeComponent();
        }

        private void btn_Cancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btn_Login_Click(object sender, EventArgs e)
        {
            //Opens up the main form
            Maxxis_Hotel_MainForm newform = new Maxxis_Hotel_MainForm();
            this.Hide();
            newform.ShowDialog();
            this.Show();
        }

        private void Maxxis_Hotel_LoginForm_Load(object sender, EventArgs e)
        {

        }
    }
}
